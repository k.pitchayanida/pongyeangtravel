<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <meta name="robots" content="noindex,nofollow" />
   <title>Pongyeang Travel : Admin</title>
   <?php
   include 'loadcss.php';
   ?>
</head>

<body class="font-thaisan">
   <?php
   include 'header.php';
   $page = isset($_GET['page']) ? $_GET['page'] : '';

   if(User::isLogin() === true) {
      if($user['usertype'] == 'admin') {
         // router
         if($page == 'newOwner') {
            include 'admin/new_user.php';
         } else if($page == 'ownerList') {
            include 'admin/list_menber_for_manager.php';
         } else if($page == 'editOwner') {
            include 'admin/edit_user.php';
         } else if($page == 'newArticle') {
            include 'admin/newArticle.php';
         } else if($page == 'manageArticle') {
             include 'admin/manageArticle.php';
         } else if($page == 'managePR') {
            include 'admin/managePR.php';

         } else if($page == 'editArticle') {
            include 'admin/editArticle.php';
         } else if($page == 'pinAll') {
            include 'admin/pinAll.php';
         } else if($page == 'pinTravel') {
            include 'admin/pinpost/pinTravel.php';
         } else if($page == 'pinVillage') {
            include 'admin/pinpost/pinVillage.php';
         } else if($page == 'pinRest') {
            include 'admin/pinpost/pinRest.php';
         } else if($page == 'pinRestaurant') {
            include 'admin/pinpost/pinRestaurant.php';
         } else if($page == 'password') {
            $editAdmin = true;
            include 'admin/edit_user.php';
         } else {
            include 'error_404.php';
         }
         // end router
      } else {
         // permission denied
         header('Location: index.php');
         exit();
      }
   } else {
      include 'login.php';
   }
   ?>

   <?php include 'footer.php'; ?>
</body>
</html>
