<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow"/>
    <title>Pongyeang Travel : Owner</title>
    <?php
    include 'loadcss.php';
    ?>
</head>

<body class="font-thaisan">
<?php
include 'header.php';
$page = isset($_GET['page']) ? $_GET['page'] : '';

if (User::isLogin() === true) {
    if ($user['usertype'] == 'owner') {
        // router
        if ($user['status'] != 'Active') {
            if ($user['status'] == 'Unactive') {
                include 'owner/profile.php';
            } else {
                // force user to edit password
                include 'owner/editProfile.php';
            }
        } else {
            if ($page == 'profile') {
                include 'owner/profile.php';
            } else if ($page == 'editProfile') {
                include 'owner/editProfile.php';
            } else if ($page == 'managePR') {
                include 'owner/managePR.php';
            } else if ($page == 'changeTypePR') {
                include 'owner/changeTypePR.php';
            } else if ($page == 'addArticle') {
                include 'owner/addArticle.php';
            } else if ($page == 'editArticle') {
                include 'owner/editArticle.php';
            } else if ($page == 'manageArticle') {
                include 'owner/manageArticle.php';
            } else if ($page == 'newPR') {
                // sub route
                $category = isset($_GET['category']) ? $_GET['category'] : '';
                if ($category == 'travel') {
                    include 'owner/newPR_travel.php';
                } else if ($category == 'rest') {
                    include 'owner/newPR_rest.php';
                } else if ($category == 'eat') {
                    include 'owner/newPR_eat.php';
                } else {
                    include 'error_404.php';
                }
            } else {
                include 'error_404.php';
            }
        }
        // end router
    } else {
        // permission denied
        header('Location: index.php');
        exit();
    }
} else {
    include 'login.php';
}
?>
<?php include 'footer.php'; ?>
</body>
</html>
