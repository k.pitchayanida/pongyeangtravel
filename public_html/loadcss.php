<style>
.page-loader {
   border: 16px solid #f3f3f3;
   border-top: 16px solid #295e52;
   border-radius: 50%;
   width: 120px;
   height: 120px;
   animation: spin 2s linear infinite;
   position: fixed;
   top: 50%;
   left: 50%;
   margin-top: -60px;
   margin-left: -60px;
   z-index: 999;
}
.page-loader-overlay {
   position: fixed;
   width: 100%;
   height: 100vh;
   background-color: #ffffff;
   z-index: 999;
}
.page-loader-overlay  img {
   width: 200px;
   position: fixed;
   top: 50%;
   left: 50%;
   margin-top: -100px;
   margin-left: -100px;
}

@keyframes spin {
   0% { transform: rotate(0deg); }
   100% { transform: rotate(360deg); }
}
</style>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/font.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Plugin CSS -->
<link rel="stylesheet" href="css/sweetalert2.min.css?v=2">
<link rel="stylesheet" href="css/datepicker.min.css">
<link rel="stylesheet" href="css/datatables.min.css">
<link rel="stylesheet" href="css/toastr.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="shortcut icon" href="images/LOGO-pongyang.png">

<!-- CUSTOM STYLE -->
<link rel="stylesheet" href="css/style.css?v=<?php echo mt_rand();?>">
<link rel="stylesheet" href="css/update-style1.css?v=<?php echo mt_rand();?>">
<link rel="stylesheet" href="css/helper.css">
