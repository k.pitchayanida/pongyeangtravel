<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <title>Pongyeang Travel : ติดต่อเรา</title>
   <?php
   include 'loadcss.php';
   ?>
</head>

<body class="font-thaisan">
   <?php
   include 'header.php';
   ?>
   <div class="container" style="margin-bottom: 20px;" id="travel_hit">
      <div class="row">
         <div class="col-xs-12">
            <div class="row header2">
               <div class="col-xs-12 border-bottom2">
                  <div class="col-xs-12 no-padding">
                     <h2 class="font-size-20 bold text-green"><a href="about.php">ติดต่อเรา</a></h2>
                  </div>
               </div>
            </div>
            <div class="row body-yellow card" style="padding: 50px;">
               <div class="col-sm-offset-1 col-sm-10">
                  <address>
                     <div class="col-sm-12 font-size-18">
                        <div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 <i class="fa fa-circle fa-stack-2x" style="color:#673ab7;"></i>
                                 <i class="fa fa-address-book-o fa-stack-1x" style="color:#ffffff;"></i>
                              </span> ที่อยู่
                           </div>
                           <div class="col-sm-7 col-xs-offset-1"> งานพัฒนาการท่องเที่ยว องค์การบริหารส่วนตำบลโป่งแยง<br />
                              191 หมู่ที่ 2 บ้านโป่งแยง ถนน แม่ริม-สะเมิง<br />
                              ตำบลโป่งแยง อำเภอแม่ริม จังหวัด เชียงใหม่               
						   </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 <i class="fa fa-circle fa-stack-2x" style="color:#4e4848;"></i>
                                 <i class="fa fa-phone fa-stack-1x" style="color:#ffffff;"></i>
                              </span> เบอร์โทรศัพท์
                           </div>
                           <div class="col-sm-7 col-xs-offset-1">053-106553 ต่อ 11  (For English  : 0813666740)  </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 <i class="fa fa-circle fa-stack-2x" style="color:#ff9800;"></i>
                                 <i class="fa fa-envelope-o fa-stack-1x" style="color:#ffffff;"></i>
                              </span> Email
                           </div>
                           <div class="col-sm-7 col-xs-offset-1"> info@pongyeang.go.th , pongyeangtravel@gmail.com </div>
                        </div>
                        
                        <div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 
                                 <i>
								 <img src="images/line_icon.png" width="40px" style="margin-left:5px; margin-right:4px;"> </i>
                              </span> LINE สอบถามการใช้งานเว็บกับแอดมิน
                       </div>
                           <div class="col-sm-7 col-xs-offset-1"><a href="https://line.me/R/ti/p/%40dca0008t" target="_blank">LINE คลิกแอดไลน์อัตโนมัติ </a> หรือไอดีไลน์ @dca0008t  <br> ID Line : 0813666740 </div>
                        </div>
                      
                        <div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 <i class="fa fa-circle fa-stack-2x" style="color:#4267b2;"></i>
                                 <i class="fa fa-facebook fa-stack-1x" style="color:#ffffff;"></i>
                              </span> Facebook
                           </div>
                           <div class="col-sm-7 col-xs-offset-1"><a href="https://www.facebook.com/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%97%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B9%82%E0%B8%9B%E0%B9%88%E0%B8%87%E0%B9%81%E0%B8%A2%E0%B8%87-174648655915338/" target="_blank">การท่องเที่ยวโปแยง</a> &nbsp;&nbsp; <a href="https://www.facebook.com/ReviewPongyeang/" target="_bank">   รีวิว โป่งแยง</a></div>
                        </div>

						<div class="row">
                           <div class="col-sm-5">
                              <span class="fa-stack">
                                 <i class="fa fa-circle fa-stack-2x" style="color:#ee4e44;"></i>
                                 <i class="fa fa-google-plus fa-stack-1x" style="color:#ffffff;"></i>
                              </span> Google Drive
                           </div>
                           <div class="col-sm-7 col-xs-offset-1"><a href="https://goo.gl/FgQaFQ" target="_blank">วิธีการใช้งานระบบเว็บไซต์ส่งเสริมการท่องเที่ยวโป่งแยง </a></div>
                        </div>


                     </div>
                  </address>
               </div>
            </div>
         </div>
      </div>
   </div>

   <?php include 'footer.php'; ?>
</body>
</html>
