<?php
include_once 'shared/setting.php';
$id = isset($_GET['id']) ? $_GET['id'] : -1;
$id = ($id >= 0 && $id < count(Setting::$village_list)) ? $id : -1;

if($id != -1) {
	$title = Setting::$village_list[$id];
	$pageLink = 'village.php?id='.$id;
} else {
	$title = 'หมู่บ้านทั้งหมดในโป่งแยง';
	$pageLink = 'village.php';
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Pongyeang Travel : <?php echo $title;?></title>
	<?php
	include 'loadcss.php';
	?>
</head>

<body class="font-thaisan">
	<?php
	include 'header.php';
	?>
	<div class="container">
		<div class="row card">
			<div class="col-xs-12">
				<div class="row header2">
					<div class="col-xs-12 border-bottom2">
						<div class="col-xs-12 no-padding">
							<h1 class="font-size-20 bold">
								<a href="village.php">เกี่ยวกับหมู่บ้าน</a> >
								<?php
								echo '<a href="'.$pageLink.'">'.$title.'</a>';
								?>
							</h1>
						</div>
					</div>
				</div>
				<?php
				if($id != -1) {
					$villageID = $id;
					include 'village/view.php';
				} else {
					include 'village/all.php';
				}
				?>
			</div>
		</div>
	</div>
	<?php include 'footer.php'; ?>
</body>
</html>
