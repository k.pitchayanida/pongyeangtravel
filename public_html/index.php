<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name=”googlebot” content=”index, follow”>
  <title>Pongyeang Travel</title>
  <?php
    include 'loadcss.php';
    ?>
</head>

<body class="font-thaisan">
  <?php
include 'header.php';
include_once 'shared/index_resource.php';
$GLOBALS['substringLength'] = 40;
?>
    <div class="container">
      <div class="row card">
        <div class="col-xs-12" style="padding-bottom: 10px">
          <div class="row header1">
            <div class="col-xs-12 border-bottom1">
              <div class="col-xs-8 no-padding">
                <h2 class="font-size-20 bold">ท่องเที่ยวยอดฮิต</h2>
              </div>
              <div class="col-sm-4 text-right no-padding">
                <h2 class="font-size-18">
                  <a href="travel_hit.php">ดูทั้งหมด >></a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 half-glutter hidden-xs" style="margin-top: 5px">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <?php
                        $travel_hit = Resource::travel_hit(6);
                        for ($i = 0; $i < count($travel_hit); $i++) {
                            if ($i == 0) {
                                echo '<div class="item active">';
                            } else {
                                echo '<div class="item">';
                            }
                            /*echo '<a href="'.$travel_hit[$i]['link'].'"><img class="img-responsive" src="'.$travel_hit[$i]['image'].'" width="100%"></a>
                            </div>';*/
                            echo '<a href="' . $travel_hit[$i]['link'] . '" title="'.$travel_hit[$i]['name'].'"><div class="image carousel-image" style="background-image:url(\'' . $travel_hit[$i]['image'] . '\');"></div>
								</a>
								</div>';
                        }
                        ?>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 half-glutter">
            <div class="row" style="margin-top: 5px">
              <?php
                    for ($i = 0; $i < count($travel_hit) && $i < 6; $i++) {
                        echo '<div class="col-md-4 col-sm-4 half-glutter">
							<a href="' . $travel_hit[$i]['link'] . '" title="'.$travel_hit[$i]['name'].'">';
                        //echo '<img src="" data-src="'.$travel_hit[$i]['image'].'" alt="" style="width: 100%" class="lazyload">';
                        echo '<div class="image list-image" style="background-image:url(\'' . $travel_hit[$i]['image'] . '\');"></div>';
                        echo '<p class="font-size-16 font-green bold single-line">' . $travel_hit[$i]['name'] . '&nbsp;</p>
                            <p class=" font-helvetica font-size-10">
                            '.Setting::$village_list[$travel_hit[$i]['village']].'<br>
                            เข้าชม: ' . $travel_hit[$i]['count_view'] . '</p>
							</a>
							</div>';
                    }
                    ?>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="container">
      <div class="row card">
        <div class="col-xs-12 ">
          <div class="row header2">
            <div class="col-xs-12 border-bottom2">
              <div class="col-xs-8 no-padding">
                <h2 class="font-size-20 bold">แนะนำที่เที่ยวเด่นจากหมู่บ้าน</h2>
              </div>
              <div class="col-xs-4 text-right no-padding">
                <h2 class="font-size-18">
                  <a href="village.php">ดูทั้งหมด >></a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-xs-12 no-padding" style="margin-top: 10px">
            <?php
                $village_hit = Resource::village_hit(16);
                if (count($village_hit) == 0) {
                    echo '<div class="col-xs-12 body-yellow">
						<p class="text-center">ไม่มีข้อมูลสถานที่ท่องเที่ยวในหมู่บ้าน</p>
						</div>';
                } else {
                    echo '<div class="col-xs-12 no-padding">
						<div class="well no-padding">
						<div id="slideThumb" class="carousel slide">
						<!-- Carousel items -->
						<div class="carousel-inner">';
                    $j = 0;
                    for ($i = 0; $i < count($village_hit); $i++) {
                        if ($j == 0) {
                            if ($i == 0) {
                                echo '<div class="item active">';
                            } else {
                                echo '<div class="item">';
                            }
                        }
                        echo '<div class="col-sm-3 half-glutter">
								<a href="' . $village_hit[$i]['link'] . '" title="'.$village_hit[$i]['name'].'" class="thumbnail">';
                        //echo '<img src="'.$village_hit[$i]['image'].'" alt="Image" style="width:100%;" />';
                        echo '<div class="image carousel-mini-image" style="background-image:url(\'' . $village_hit[$i]['image'] . '\');"></div>';
                        echo '<h4 class="single-line">' . $village_hit[$i]['name'] . '&nbsp;</h4>
								<p class=" font-helvetica font-size-10">' . Setting::$village_list[$village_hit[$i]['village']] . '</p>
								<p class=" font-helvetica font-size-10">เข้าชม ' . $village_hit[$i]['count_view'] . ' ครั้ง</p>
								</a>
								</div>';
                        if ($j == 3 || $i == count($village_hit) - 1) {
                            $j = 0;
                            echo '</div><!--/item-->';
                        } else {
                            $j++;
                        }
                    }
                    echo '</div><!--/carousel-inner-->
							<a data-slide="prev" href="#slideThumb" class="izq carousel-control">‹</a>
							<a data-slide="next" href="#slideThumb" class="der carousel-control">›</a>
							</div><!--/myCarousel-->

							</div><!--/well-->
							</div>';
                }
                ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row card">
        <div class="col-xs-12">
          <div class="row header1">
            <div class="col-xs-12 border-bottom1">
              <div class="col-xs-8 no-padding">
                <h2 class="font-size-20 bold">ข่าวสารและกิจกรรม</h2>
              </div>
              <div class="col-xs-4 text-right no-padding">
                <h2 class="font-size-18">
                  <a href="event.php?category=festival">ดูทั้งหมด >></a>
                </h2>
              </div>
            </div>
          </div>
          <div class="row body-yellow">
            <div class="col-xs-12">
              <?php
                    $events = Resource::article(Setting::article_category_index('festival'), 12);
                    echo '<div class="row text-center" style="margin-top: 10px">';
                    for ($i = 0; $i < count($events) && $i < 12; $i++) {
                        echo '<div class="col-md-2 col-sm-3 half-glutter">
								<a href="' . $events[$i]['link'] . '" title="'.$events[$i]['name'].'">';
                        //echo '<img src="" data-src="'.$events[$i]['image'].'" style="width: 100%" class="lazyload">';
                        echo '<div class="image list-image" style="background-image:url(\'' . $events[$i]['image'] . '\');"></div>';
                        echo '<p class="font-green single-line">' . $events[$i]['name'] . '&nbsp;</p>
								<p class=" font-helvetica font-size-10">' . Setting::$village_list[$events[$i]['village']] . '</p>
								</a>
								</div>';
                    }
                    echo '</div>';

                    if (count($events) == 0) { //							NO ARTICLE
                        echo '<div class="col-xs-12 body-yellow">
								<p class="text-center">ไม่มีข้อมูลข่าวสารและกิจกรรม</p>
								</div>';
                    }
                    ?>
            </div>
          </div>

        </div>

      </div>
    </div>

    <div class="container">
      <div class="row card">
        <div class="col-xs-12">
          <div class="row header1">
            <div class="col-xs-12 border-bottom1">
              <div class="col-xs-8 no-padding">
                <h2 class="font-size-20 bold">รวมที่พักเด่น</h2>
              </div>
              <div class="col-xs-4 text-right no-padding">
                <h2 class="font-size-18">
                  <a href="hotel.php">ดูทั้งหมด >></a>
                </h2>
              </div>
            </div>
          </div>
          <div class="row body-yellow">
            <div class="col-xs-12">
              <?php
                    $hotel = Resource::hotel_hit(12);
                    echo '<div class="row text-center" style="margin-top: 10px">';
                    for ($i = 0; $i < count($hotel) && $i < 12; $i++) {
                        echo '<div class="col-md-2 col-sm-3 half-glutter">
								<a href="' . $hotel[$i]['link'] . '" title="'.$hotel[$i]['name'].'">';
                        //echo '<img src="" data-src="'.$events[$i]['image'].'" style="width: 100%" class="lazyload">';
                        echo '<div class="image list-image" style="background-image:url(\'' . $hotel[$i]['image'] . '\');"></div>';
                        echo '<p class="font-green single-line">' . $hotel[$i]['name'] . '&nbsp;</p>
								</a>
								</div>';
                    }
                    echo '</div>';

                    if (count($hotel) == 0) { //							NO ARTICLE
                        echo '<div class="col-xs-12 body-yellow">
								<p class="text-center">ไม่มีข้อมูลที่พัก</p>
								</div>';
                    }
                    ?>
            </div>
          </div>

        </div>

      </div>
    </div>

    <div class="container">
      <?php
    $restaurant = Resource::restaurant_hit(6);
    function showRestaurant($resource, $index)
    {
        global $substringLength;
        if ($index < count($resource)) {
            echo '<a href="' . $resource[$index]['link'] . '" title="'.$resource[$index]['name'].'">
					<div class="col-md-4 col-sm-6 no-padding-right">';
            //echo '<img src="" class="lazyload" data-src="'.$resource[$index]['image'].'" style="width: 100%">';
            echo '<div class="image list-image" style="background-image:url(\'' . $resource[$index]['image'] . '\');"></div>';
            echo '<p class="font-green single-line">' . $resource[$index]['name'] . '&nbsp;</p>
					<p class=" font-helvetica font-size-10 single-line">' . iconv_substr($resource[$index]['description_short'], 0, $substringLength, "UTF-8") . '</p>
					</div>
					</a>';
        }
    }

    ?>
        <div class="row header1">
          <div class="col-xs-12 border-bottom1">
            <div class="col-xs-6 no-padding">
              <h2 class="font-size-20 bold">อร่อยด้วยกัน</h2>
            </div>
            <div class="col-xs-6 text-right no-padding">
              <h2 class="font-size-18">
                <a href="restaurant.php">ดูทั้งหมด >></a>
              </h2>
            </div>
          </div>
        </div>
        <div class="row card">
          <div class="col-xs-12 body-yellow" style="padding: 10px 0px 10px 0px;">
            <div class="col-sm-6 half-glutter" style="margin-bottom:10px;">
              <div id="foodCarousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <?php
                        for ($i = 0; $i < count($restaurant) && $i < 6; $i++) {
                            if ($i == 0) {
                                echo '<div class="item active">';
                            } else {
                                echo '<div class="item">';
                            }
                            echo '<a href="' . $restaurant[$i]['link'] . '" title="'.$restaurant[$i]['name'].'">';
                            //echo '<img class="img-responsive" src="'.$restaurant[$i]['image'].'" width="100%">';
                            echo '<div class="image carousel-image" style="background-image:url(\'' . $restaurant[$i]['image'] . '\');"></div>';
                            echo '</a>
									</div>';
                        }
                        ?>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#foodCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#foodCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <div class="col-sm-6 half-glutter">
              <div class="row" style="margin-right: 0px;">
                <?php
                    if (count($restaurant) == 0) {
                        echo '<div class="col-xs-12 body-yellow">
								<p class="text-center">ไม่มีข้อมูลร้านค้า/ร้านอาหาร</p>
								</div>';
                    }
                    showRestaurant($restaurant, 0);
                    showRestaurant($restaurant, 1);
                    showRestaurant($restaurant, 2);
                    showRestaurant($restaurant, 3);
                    showRestaurant($restaurant, 4);
                    showRestaurant($restaurant, 5);
                    ?>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="container">
      <div class="row header2">
        <div class="col-xs-12 border-bottom2">
          <div class="col-xs-8 no-padding">
            <h2 class="font-size-20 bold">แนะนำโปรแกรมท่องเที่ยว</h2>
          </div>
          <div class="col-xs-4 text-right no-padding">
            <h2 class="font-size-18">
              <a href="event.php?category=travel">ดูทั้งหมด >></a>
            </h2>
          </div>
        </div>
      </div>
      <div class="row card" style="padding-top: 10px;">
        <?php
        $travel_guide = Resource::article(Setting::article_category_index('travel'), 8);
        if (count($travel_guide) == 0) {
            echo '<div class="col-xs-12 body-yellow">
					<p class="text-center">ไม่มีข้อมูลแนะนำโปรแกรมท่องเที่ยว</p>
					</div>';
        }
        for ($i = 0; $i < count($travel_guide) && $i < 8; $i++) {
            echo '<a href="' . $travel_guide[$i]['link'] . '" title="'.$travel_guide[$i]['name'].'"><div class="col-md-3 col-sm-6 half-glutter">';
            //echo '<img src="" class="lazyload" data-src="'.$travel_guide[$i]['image'].'" style="width: 100%">';
            echo '<div class="image carousel-mini-image" style="background-image:url(\'' . $travel_guide[$i]['image'] . '\');"></div>';
            echo '<p class="font-green single-line">' . $travel_guide[$i]['name'] . '</p>
					<p class=" font-helvetica font-size-10 single-line">' . iconv_substr($travel_guide[$i]['description_short'], 0, $substringLength, "UTF-8") . '&nbsp;</p>
					</div></a>';
        }
        ?>
      </div>
    </div>
    <div class="container">
      <div class="row header1">
        <div class="col-xs-12 border-bottom1">
          <div class="col-xs-8 no-padding">
            <h2 class="font-size-20 bold">บทความแนะนำกับรีวิวท่องเที่ยว</h2>
          </div>
          <div class="col-xs-4 text-right no-padding">
            <h2 class="font-size-18">
              <a href="event.php?category=review">ดูทั้งหมด >></a>
            </h2>
          </div>
        </div>
      </div>
      <div class="row card" style="padding-top: 10px;">
        <?php
        $review = Resource::article(Setting::article_category_index('review'), 8);
        if (count($review) == 0) {
            echo '<div class="col-xs-12 body-yellow">
					<p class="text-center">ไม่มีข้อมูลบทความแนะนำกับรีวิวท่องเที่ยว</p>
					</div>';
        }
        for ($i = 0; $i < count($review) && $i < 8; $i++) {
            echo '<a href="' . $review[$i]['link'] . '" title="'.$review[$i]['name'].'"><div class="col-md-3 col-sm-6 half-glutter">';
            //echo '<img src="" class="lazyload" data-src="'.$review[$i]['image'].'" style="width: 100%">';
            echo '<div class="image carousel-mini-image" style="background-image:url(\'' . $review[$i]['image'] . '\');"></div>';
            echo '<p class="font-green single-line">' . $review[$i]['name'] . '</p>
					<p class=" font-helvetica font-size-10 single-line">' . iconv_substr($review[$i]['description_short'], 0, $substringLength, "UTF-8") . '&nbsp;</p>
					</div></a>';
        }
        ?>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
        <div class="col-sm-offset-3 col-sm-6 col-xs-12 border-bottom1"></div>
        <div class="col-xs-12">&nbsp;</div>
      </div>
    </div>

    <?php include 'footer.php'; ?>
</body>

</html>