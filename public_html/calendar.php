<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Pongyeang Travel : ปฎิทินการท่องเที่ยว</title>
	<!-- SEO meta tags -->
	<meta name="keywords" content="โป่งแยง,ปฏิทินการท่องเที่ยว">
	<meta name="description" content="ปฎิทินการท่องเที่ยวโป่งแยง" >
	<?php
	include 'loadcss.php';
	?>
	<link rel="stylesheet" href="css/fullcalendar.min.css">
	<link rel="stylesheet" href="css/jquery.qtip.min.js.css">
	<style>
		.qtip-content{
			font-size: 20px;
		}

		.qtipDisplayArticle {
			padding: 3px;
		}
	</style>
</head>

<body class="font-thaisan">
	<?php
	include 'header.php';
	?>
	<div class="container" style="margin-bottom: 20px;">
		<div class="row card">
			<div class="col-xs-12 ">
				<div class="row header2">
					<div class="col-xs-12 border-bottom2">
						<div class="col-xs-12 ">
							<h1 class="font-size-20 bold">ปฎิทินการท่องเที่ยวกิจกรรม >
								<a href="travel.php">เทศกาลและกิจกรรม</a>
							</h1>
						</div>
					</div>
				</div>
				<div class="col-xs-12" style="text-align: center;">
					<div class="row">
						<div class="col-xs-12" style="text-align: center;">
							<h3>คลิกที่ปฎิทินเพื่อดูรายละเอียด</h3>
							<hr>
						</div>
						<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include 'footer.php'; ?>
	<script src="js/moment.js"></script>
	<script src="js/fullCalendar/fullcalendar.min.js"></script>
	<script src="js/fullCalendar/locale-all.js"></script>
	<script src="js/jquery.qtip.min.js"></script>
	<script>
	$(document).ready(function() {
		var calendar = null;
		var $calendar = $('#calendar');
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		$calendar.on( 'click', '.fc-event', function (e){
			e.preventDefault();
			var href = $(this).attr('href');
			if(!! href) window.open(href, '_blank');
		});
		$calendar.on( 'click', '.fc-button', function (e){
			var isPrevBtn = $(this).hasClass('fc-prev-button');
			var isNextBtn = $(this).hasClass('fc-next-button');
			var isTodayBtn = $(this).hasClass('fc-today-button');
			if(!isPrevBtn && !isNextBtn && !isTodayBtn) {
				$('.fc-button').removeClass('fc-state-active');
				$(this).addClass('fc-state-active');
			}
		});

		calendar = $calendar.fullCalendar({
			locale: 'th',
			customButtons: {
				myDay: {
					text: 'วัน',
					click: function() {
						calendar.fullCalendar('changeView', 'listDay');
					}
				},
				myWeek: {
					text: 'สัปดาห์',
					click: function() {
						calendar.fullCalendar('changeView', 'listWeek');
					}
				},
				myYear: {
					text: 'ปี',
					click: function() {
						calendar.fullCalendar('changeView', 'listYear');
					}
				}
			},
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,myWeek,myDay'
			},
			editable: false,
			events: {
				url: 'public/article/ajax.event_calendar.php',
				type: 'POST'
			},
			eventRender: function(event, element) {
				element.qtip({
					content: event.description
				});
			}
		});

		setTimeout(function() {
			// wati until page fade in
			calendar.fullCalendar('render');
		}, 800);
	});
	</script>
</body>
</html>
