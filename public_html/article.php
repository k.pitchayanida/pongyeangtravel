<?php
include_once 'shared/setting.php';

$id = $_GET['id'];
$article = Amst::get(Setting::$section_code . '_article', '*', array('id' => $id));
$isAdmin = false;
if (!$article) {
    header('Location: event.php');
    exit();
}
$loginUser = User::getUserByID(User::getCurrentUserid());
if ($article['flag_published'] != 1 && $loginUser['usertype'] != Setting::$user_type['admin']) {
    header('Location: event.php');
    exit();
}

if ($loginUser['usertype'] == Setting::$user_type['admin']) {
    $isAdmin = true;
}

$category = $article['category'];
if (!($category >= 0 && $category <= count(Setting::$article_category))) {
    $category = -1;
}
$title = array();
for ($i = 0; $i < count(Setting::$article_category); $i++) {
    array_push($title, ' > <a href="event.php?category=' . Setting::$article_category_key[$i] . '">' .
        Setting::$article_category[$i] . '</a>');
}

$images = Amst::select(Setting::$section_code . '_article_image', '*', array(
    'AND' => array(
        'article_id' => $id,
        'status' => 'Active'
    )
));
$pageLink = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8">
    <title>Pongyeang Travel :
      <?php echo $article['name']; ?>
    </title>
    <!-- SEO meta tags -->
    <meta name="keywords" content="<?php echo $article['hashtag']; ?>">
    <meta name="description" content="<?php echo $article['description_short']; ?>">

    <!-- Open Graph data -->
    <meta property="og:title" content="Pongyeang Travel : <?php echo $article['name']; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $article['description_short']; ?>" />
    <meta property="og:site_name" content="Pongyeang Travel" />
    <?php if ($images) {
        $imageID = $images[0]['id'];
        $imagePath = File::getPath(Setting::$section_code . '_article_' . $imageID, 'system/');
        if (!$imagePath) {
            $imagePath = 'images/system/noimage.jpg';
        }
        echo '<meta property="og:image" content="' . $imagePath . '" />';
    }
    include 'loadcss.php';
    ?>
    <link rel="stylesheet" href="css/ekko-lightbox.min.css">
  </head>

  <body class="font-thaisan">
    <?php
include 'header.php';
$user = User::getUserByID($article['insertuserid']);
?>

      <div class="container">
        <div class="row card">
          <div class="col-xs-12 ">
            <div class="row header2">
              <div class="col-xs-12 border-bottom2">
                <div class="col-xs-12 no-padding">
                  <?php
                        if ($isAdmin) { ?>
                    <h1 class="font-size-20 bold">Admin จัดการ >
                      <a href="admin.php?page=manageArticle">จัดการบทความ</a> >
                      <?php echo $article['name']; ?>

                    </h1>
                    <?php } else {
                            ?>
                    <h1 class="font-size-20 bold">
                      <a href="event.php">กิจกรรมการท่องเที่ยว</a>
                      <?php if ($category != -1) echo $title[$category]; ?>
                    </h1>
                    <?php } ?>
                </div>
              </div>
            </div>
            <div class="font-size-14" style="margin-top: 20px">
              <div calss="col-xs-12 half-glutter">
                <u class="pull-right">บทความโดย
                  <?php echo $user['name']; ?>
                </u>
              </div>
              <div class="row border-bottom2">
                <div class="col-xs-12">
                  <h2 class="text-green" style="text-align: center;">
                    <?php echo $article['name']; ?>
                  </h2>

                  <h3 class="text-green" style="text-align: center;">
                    <?php echo $article['description_short']; ?>
                  </h3>
                </div>
                <div class="col-sm-12 no-padding">
                  <?php if ($images) { ?>
                  <div class="row " style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="col-xs-12">
                      <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <?php
                                            for ($i = 0; $i < count($images); $i++) {
                                                $imageID = $images[$i]['id'];
                                                $imagePath = File::getPath(Setting::$section_code . '_article_' . $imageID, 'system/');
                                                if (!$imagePath) {
                                                    $imagePath = 'images/system/noimage.jpg';
                                                }
                                                if ($i == 0) {
                                                    echo '<div class="item active">
                            <div class="img-16by9" style="background-image: url('.$imagePath.');"></div>
														</div>';
                                                } else {
                                                    echo '<div class="item">
														<div class="img-16by9" style="background-image: url('.$imagePath.');"></div>
														</div>';
                                                }
                                            }
                                            ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <div class="col-xs-12 half-glutter">
                <p class="font-size-16">
                  <?php echo $article['description']; ?>
                </p>
              </div>
              <div class="col-xs-12 border-bottom2" style="margin-bottom: 20px;">
                <?php
                    for ($i = 0; $i < count($images); $i++) {
                        echo '<div class="row" style="margin-top: 20px;">';
                        for ($j = $i; $j < count($images); $j++) {
                            $imageID = $images[$i]['id'];
                            $imagePath = File::getPath(Setting::$section_code . '_article_' . $imageID, 'system/');
                            if (!$imagePath) {
                                $imagePath = 'images/system/noimage.jpg';
                            }
                            echo '<div class="col-sm-4 half-glutter">
									<a class="list-quotes" href="' . $imagePath . '" data-toggle="lightbox" data-gallery="article-gallery" data-title="' . $images[$i]['caption'] . '">
									<img class="img-4by3" style="background-image: url(' . $imagePath . ');">
									<div class="quotes">
									</div>
									</a>
									</div>';
                            $i++;
                        }
                        echo '</div>';
                    }
                    ?>
              </div>
              <div class="col-xs-12">
                <div class="row" style="margin-bottom: 20px;">
                  <div class="col-sm-6 font-size-18">
                    <div class="row">
                      <div class="col-sm-5 bold">วันที่จัดกิจกรรม</div>
                      <div class="col-sm-7 col-xs-offset-1">
                        <?php echo date('d/m/Y', strtotime($article['date_start'])) . ' ถึง ' . date('d/m/Y', strtotime($article['date_end'])); ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5 bold">ช่วงเวลากิจกรรม</div>
                      <div class="col-sm-7 col-xs-offset-1">
                        <?php echo date('H:i', strtotime($article['time_start'])) . ' ถึง ' . date('H:i', strtotime($article['time_end'])) . ' น.'; ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5 bold">สิ่งอำนวยความสะดวก</div>
                      <div class="col-sm-7 col-xs-offset-1">
                        <?php echo $article['facilities']; ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5 bold">หมู่บ้าน</div>
                      <div class="col-sm-7 col-xs-offset-1">
                        <?php echo Setting::$village_list[$article['village']]; ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-5 bold">ติดต่อสอบถามข้อมูลได้ที่</div>
                      <div class="col-sm-7 col-xs-offset-1">
                        <?php echo $article['info_contact']; ?>
                      </div>
                    </div>
                  </div>
                  <?php if (trim($article['hashtag']) != '') { ?>
                  <h4>Hashtag โดนๆ</h4>
                  <div class="col-sm-6 well body-yellow">
                    <div>
                      <?php
                                    if (trim($article['hashtag']) != '') {
                                        $hashtags = explode(',', trim($article['hashtag']));
                                        for ($i = 0; $i < count($hashtags); $i++) {
                                            $tag = trim($hashtags[$i]);
                                            echo '<a href="search.php?q=' . $tag . '" class="btn btn-green font-size-14">' . $tag . '</a> ';
                                        }
                                    }
                                    ?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <!-- Edit Article For Owner-->
                <?php

                    if ($loginUser['id'] == $article['insertuserid']) {
                        echo '<div class="row text-center" style="margin-bottom: 20px;">
									<a href="owner.php?page=editArticle&id=' . $article['id'] . '" class="btn btn-warning font-size-14"><i class="fa fa-edit"></i> แก้ไข</a>
									<a  class="btn btn-danger togglestatus font-size-14" data-id="' . $article['id'] . '"><i class="fa fa-trash"></i> ลบ</a>
									</div>';
                    } else {
                        if ($loginUser['usertype'] == Setting::$user_type['admin']) {
                            echo '<div class="row text-center" style="margin-bottom: 20px;">
									<a href="admin.php?page=editArticle&id=' . $article['id'] . '" class="btn btn-warning font-size-14"><i class="fa fa-edit"></i> แก้ไข</a>
									</div>';
                        }
                    }
                    ?>
              </div>

            </div>
          </div>
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12 text-center">
                <hr/>
                <div class="addthis_inline_share_toolbox"></div>
                <hr/>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <!-- Page Plugun -->
      <script src="js/ekko-lightbox.min.js"></script>
      <script>
        $(document).ready(function () {
          $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
          });
          $(document).on('click', '.togglestatus', function (e) {
            var $this = $(this);
            var id = $this.data('id');
            System.helper.confirm('', 'คุณต้องการลบ Article นี้?', function () {
              approvePR(id, $this);
            });
          });

          approvePR = function (id, $btn) {
            $.ajax({
                url: 'owner/ajax.deleteArticle.php',
                type: 'POST',
                dataType: 'json',
                data: {
                  'id': id
                }
              })
              .done(function (result, status, xhr) {
                if (result.error == false) {
                  System.helper.notification('success', 'บันทึกข้อมูลสำเร็จ');
                  table.ajax.reload();
                } else {
                  System.helper.notification('error', result.message);
                }
              });
          }
        });
      </script>


      <!-- Social share -->
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59182513c3af237f"></script>
      <?php include 'footer.php'; ?>
  </body>

  </html>