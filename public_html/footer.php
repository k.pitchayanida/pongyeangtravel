<footer class="background-footer" id="footer">
	<div class="footer-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-4 no-padding-right">
					<img width="80%" src="images/LOGO-pongyang_w.png" style="max-width: 150px;">
				</div>
				<div class="col-xs-2 no-padding hidden-xs">
					<a href="/">หน้าแรก</a><br>
					<a href="/travel.php">สถานที่ท่องเที่ยว</a><br>
					<a href="/event.php">กิจกรรมท่องเที่ยว</a><br>
					<a href="/hotel.php">ที่พัก</a><br>
					<a href="/village.php">เกี่ยวกับหมู่บ้าน</a><br>
					<a href="/restaurant.php">อร่อยด้วยกัน</a><br>
					<a href="/about.php">ติดต่อ</a><br>
				</div>
				<div class="col-sm-4 col-xs-7 no-padding-righ">
					<p>งานพัฒนาการท่องเที่ยว  องค์การบริหารส่วนโป่งแยง<br>
						191 หมู่ที่ 2 บ้านโป่งแยง ถนน แม่ริม-สะเมิง<br>
						ตำบลโป่งแยง อำเภอแม่ริม จังหวัด เชียงใหม่<br>
						โทรศัพท  : 053-106553 ต่อ 11  (For English  : 0813666740)<br>
						e-mail : info@pongyeang.go.th , pongyeangtravel@gmail.com <br>
						<a href="https://line.me/R/ti/p/%40dca0008t" target="_blank">LINE คลิกแอดไลน์อัตโนมัติ </a> หรือไอดีไลน์ 0813666740
					</p>
				</div>
				<div class="col-md-4" style="text-align:right;">
					<div class="col-xs-12 no-padding">
						<a href="#" class="social-button">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-twitter fa-stack-1x color-green"></i>
							</span>
						</a>
						<a href="https://goo.gl/FgQaFQ" class="social-button">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-google-plus fa-stack-1x color-green"></i>
							</span>
						</a>
						<a href="#" class="social-button">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-instagram fa-stack-1x color-green"></i>
							</span>
						</a>
						<a href="https://www.facebook.com/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%97%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%A7%E0%B9%82%E0%B8%9B%E0%B9%88%E0%B8%87%E0%B9%81%E0%B8%A2%E0%B8%87-174648655915338/" class="social-button">
							<span class="fa-stack fa-lg">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-facebook fa-stack-1x color-green"></i>
							</span>
						</a>
					</div>
					<div class="col-xs-12 no-padding">
						<div class="space-6"></div>
						<?php if(User::isLogin() !== true) { ?>
						<a href="#" class="showLogin"><font style="color:#fcab55;">เข้าสู่ระบบ</font></a>
						<?php } ?>
					</div>

					<div class="col-xs-10 pull-right no-padding">
						<form class="navbar-form no-padding" role="search" action="search.php" method="GET">
							<div class="input-group input-group-sm footer-search">
								<input type="text" class="form-control font-size-16" placeholder="Search Pongyeang Travel" name="q">
								<div class="input-group-btn">
									<button class="btn btn-green btn-sm" type="submit">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="col-xs-12">
						<a href="allpr.php">PR การท่องเที่ยวโป่งแยงทั้งหมด  <i class="fa fa-map-marker"></i></a>
					</div>
					<div class="col-xs-12 font-size-14">
						&#169; 2017 by website pongyeang travel
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-lg btn-green">
	<i class="ace-icon fa fa-angle-up icon-only bigger-110"></i>
</a>
<!-- End Page HTML -->
</div>

<!-- Login Modal -->
<?php
include 'login_modal.php';
$randomNumber = rand(1,10000);
?>

<!-- polyfill -->
<script src="js/promise.min.js"></script>

<!-- Load main javascript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/system.js?v=<?php echo $randomNumber;?>"></script>
<script src="js/lazyload.js"></script>

<!-- Load plugin javascript -->
<!-- Replace HTML5 form validator method by
- add data-toggle="validator" into from tag to enable plugin
- add .help-block .with-errors to element that want to display error
-->
<script src="js/validator/validator.js"></script>
<script src="js/sweetalert2/sweetalert2.min.js?v=<?php echo $randomNumber;?>"></script>
<script src="js/datepicker.min.js"></script>
<script src="js/datepicker.th.js"></script>
<script src="js/datatables.min.js"></script>
<script src="js/toastr/toastr.min.js"></script>

<!-- Google Analytics -->
<script>
	window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
	ga('create', 'UA-103110707-1', 'auto');
	ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->