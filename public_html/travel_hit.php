<!DOCTYPE html>

<html>



<head>

	<meta charset="utf-8">

	<title>Pongyeang Travel : ไปด้วยกันกับสถานที่ท่องเที่ยวยอดฮิต</title>

	<?php

	include 'loadcss.php';

	?>

</head>



<body class="font-thaisan">

	<?php

	include 'header.php';

	?>

	<div class="container" style="margin-bottom: 20px;" id="travel_hit">

		<div class="row card">

			<div class="col-xs-12 ">

				<div class="row header2">

					<div class="col-xs-12 border-bottom2">

						<div class="col-xs-12 ">

							<h2 class="font-size-20 bold text-green"><a href="travel_hit.php">ไปด้วยกันกับสถานที่ท่องเที่ยวยอดฮิต</a></h2>

						</div>

					</div>

				</div>

				<div class="font-size-14">

					<table id="travelContainer" width="100%">

                  <thead style="display: none;">

                     <tr><th></th></tr>

                  </thead>

                  <tbody>

                     <!-- travel List Here -->

                  </tbody>

               </table>

				</div>

			</div>

		</div>

	</div>

	<div class="row">

		<?php include 'footer.php'; ?>

	</div>

	<script>

	$(document).ready(function() {

		var $scope = $('#travel_hit');

		var table = null;



		initArticleList = function () {

			var $container = $scope.find('#travelContainer');

			table = $container.DataTable({

				'dom': '<tp>',

				'pagingType': 'full_numbers',

				'pageLength': 1,

				'language': System.helper.DataTable.language,

				'processing': true,

				'serverSide': true,

				'ajax': {

					'url': 'public/pr/ajax.travelHit.php',

					'type': 'POST'

				}

			});

			$scope.find('.dataTables_paginate').css('text-align', 'center');

			$scope.find('.dataTables_paginate').css('float', 'none');

		}



		initArticleList();

	});

	</script>

</body>

</html>

