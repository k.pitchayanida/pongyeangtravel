<?php
include_once 'shared/setting.php';
include_once 'shared/index_resource.php';
$pageName = basename(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), '.php');

$travelPages = array('travel', 'travel_hit');
$eventPages = array('event', 'article', 'calendar');

$pr_loggedInUser = Amst::get(Setting::$section_code . '_pr', '*', array(
    'insertuserid' => User::getCurrentUserid()
));

if (in_array($pageName, $travelPages)) {
    $pageName = 'travel';
} else if (in_array($pageName, $eventPages)) {
    $pageName = 'event';
}

if (User::isLogin() === true) {
    $user = User::getUserByID(User::getCurrentUserid());
}
?>
<div class="page-loader-overlay">
    <div class="page-loader"></div>
</div>
<script src="js/jquery-3.2.1.min.js"></script>

<!-- Page HTML -->
<div class="page-html">
    <header>
        <div class="topbar container-fulid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="container">
                <div class="logo">
                    <div class="col-sm-6">
                        <a href="/">
                            <img width="80" src="images/LOGO-pongyang.png">
                            <font class="font-size-20 bold font-brown">Pongyeang Travel</font>
                        </a>
                    </div>
                    <div class="col-sm-6" style="text-align:right; padding: 0px 15px 0px 15px;" id="topSearchContainer">
                        <form class="navbar-form" role="search" action="search.php" method="GET">
                            <div class="input-group header-search">
                                <input type="text" class="form-control" placeholder="Search Pongyeang Travel" name="q">

                                <div class="input-group-btn">
                                    <button class="btn btn-white" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <a href="calendar.php" class="font-white"><i class="fa fa-calendar color-white "></i>
                                ปฎิทินการท่องเที่ยว</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <nav class="nav">

            <div class="collapse navbar-collapse border-bottom1" style="text-align: center; line-height:0px;"
                 id="topNavbar">
                <ul class="nav navbar-nav font-size-18" style="float: none;display: inline-block; text-align: center;">
                    <li>
                        <a class="<?php echo ($pageName == 'index') ? 'active' : ''; ?>" href="index.php">หน้าแรก</a>
                    </li>
                    <li class="dropdown <?php echo ($pageName == 'travel') ? 'active' : ''; ?>">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            สถานที่ท่องเที่ยว
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu font-size-16">
                            <li><a href="travel.php">สถานที่ท่องเที่ยวทั้งหมด</a>
                            </li>
                            <?php
                            for ($i = 0; $i < count(Setting::$pr_sub_category[0]); $i++) {
                                echo '<li><a href="travel.php?category=' . Setting::$pr_sub_category_key[0][$i] . '">' . Setting::$pr_sub_category[0][$i] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="dropdown <?php echo ($pageName == 'event') ? 'active' : ''; ?>">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            กิจกรรมการท่องเที่ยว
                            <span class="caret"></span>
                        </a>
                        </a>
                        <ul class="dropdown-menu font-size-16">
                            <li><a href="event.php">กิจกรรมการท่องเที่ยวทั้งหมด</a></li>
                            <li><a href="event.php?category=festival">เทศกาลและกิจกรรม</a></li>
                            <li><a href="event.php?category=travel">โปรแกรมแนะนำการท่องเที่ยว</a></li>
                            <li><a href="event.php?category=review">บทความแนะนำกับรีวิวท่องเที่ยว</a></li>
                            <li><a href="event.php?category=knowledge">สาระความรู้</a></li>
                        </ul>
                    </li>
                    <li class="dropdown <?php echo ($pageName == 'hotel') ? 'active' : ''; ?>">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            ที่พัก
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu font-size-16">
                            <li><a href="hotel.php">ที่พักทั้งหมด</a></li>
                            <?php
                            for ($i = 0; $i < count(Setting::$pr_sub_category[1]); $i++) {
                                echo '<li><a href="hotel.php?category=' . Setting::$pr_sub_category_key[1][$i] . '">' . Setting::$pr_sub_category[1][$i] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="dropdown <?php echo ($pageName == 'restaurant') ? 'active' : ''; ?>">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            อร่อยด้วยกัน
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu font-size-16">
                            <li><a href="restaurant.php">ร้านทั้งหมด</a></li>
                            <?php
                            for ($i = 0; $i < count(Setting::$pr_sub_category[2]); $i++) {
                                echo '<li><a href="restaurant.php?category=' . Setting::$pr_sub_category_key[2][$i] . '">' . Setting::$pr_sub_category[2][$i] . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="dropdown <?php echo ($pageName == 'village') ? 'active' : ''; ?>">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            เกี่ยวกับหมู่บ้าน
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu font-size-16">
                            <li><a href="village.php">หมู่บ้านทั้งหมด</a></li>
                            <?php
                            for ($i = 0; $i < count(Setting::$village_list); $i++) {
                                echo '<li>
                     <a href="village.php?id=' . $i . '">' . Setting::$village_list[$i] . '</a>
                     </li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                    if (User::isLogin() === true && $user['usertype'] == 'admin') {
                        ?>
                        <li class="dropdown <?php echo ($pageName == 'admin') ? 'active' : ''; ?>">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                Admin จัดการ <span style="color: red;"> (<?php echo Resource::countPrWaiting(); ?>)</span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu font-size-16">
                                <li><a href="admin.php?page=password">แก้ไขรหัสผ่าน (Change Password)</a></li>
                                <li><a href="admin.php?page=newOwner">สร้างสมาชิกใหม่ (Generate Owner)</a></li>
                                <li><a href="admin.php?page=ownerList">จัดการสมาชิก (Manage Member)</a></li>
                                <li><a href="admin.php?page=managePR">รายการคำร้อง PR ใหม่ > <span id="pr-waiting" style="color: red;"> (รอ Approve <?php echo Resource::countPrWaiting(); ?> )</span></a></li>
                                <!-- <li><a href="admin.php?page=approve">รายการคำร้อง (Appove)</a></li> -->
                                <li><a href="admin.php?page=pinAll">จัดการ Pin Post ยอดนิยม (Manage Pin Post)</a></li>
                                <li style="padding-left: 15px;"><a href="admin.php?page=pinTravel">-จัดการ Pin Post
                                        สถานที่ท่องเที่ยวยอดนิยม</a></li>
                                <li style="padding-left: 15px;"><a href="admin.php?page=pinVillage">-จัดการ Pin Post
                                        สถานที่ท่องเที่ยวยอดนิยมของหมู่บ้าน</a></li>
                                <li style="padding-left: 15px;"><a href="admin.php?page=pinRest">-จัดการ Pin Post
                                        สถานที่พักยอดนิยม</a></li>
                                <li style="padding-left: 15px;"><a href="admin.php?page=pinRestaurant">-จัดการ Pin Post
                                        ร้านค้าร้านอาหารยอดนิยม</a></li>
                                <li style="padding-left: 15px;"><a href="admin.php?page=manageArticle">-จัดการ Pin Post
                                        บทความท่องเที่ยวยอดนิยม</a></li>
                                <li><a href="admin.php?page=managePR">จัดการสถานะ PR การท่องเที่ยว (Manage Status
                                        PR)</a></li>
                                <li><a href="admin.php?page=newArticle">สร้างบทความและกิจกรรมการท่องเที่ยว (Create
                                        Article)</a></li>
                                <li><a href="admin.php?page=manageArticle">จัดการบทความที่สร้าง (My Article)</a></li>
                                <li><a href="admin.php?page=managePR">จัดการ PR (Approve PR)</a></li>
                            </ul>
                        </li>
                        <?php
                    }
                    if (User::isLogin() === true && $user['usertype'] == 'owner') {
                        ?>
                        <li class="dropdown <?php echo ($pageName == 'owner') ? 'active' : ''; ?>">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                จัดการ PR
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu font-size-16">
                                <li><a href=owner.php?page=profile>ข้อมูลสมาชิก (My Profile)</a></li>
                                <li><a href="owner.php?page=editProfile">แก้ไขรหัสผ่าน (Change Password)</a></li>
                                <?php
                                /*for($i = 0; $i < count(Setting::$pr_category); $i++) {
                                echo '<li><a href="owner.php?page=newPR&category='.Setting::$pr_category_key[$i].'">สร้าง PR '.Setting::$pr_category[$i].'</a></li>';
                             }*/
                                ?>
                                <li><a href="owner.php?page=managePR">จัดการ PR การท่องเที่ยว (Manage PR)</a></li>
                                <li><a href="owner.php?page=addArticle">สร้างบทความและกิจกรรมการท่องเที่ยว (Create
                                        Article)</a></li>
                                <li><a href="owner.php?page=manageArticle">จัดการบทความที่สร้าง (My Article)</a></li>
                                <?php
                                if (isset($pr_loggedInUser["category"])) {
                                    echo '<li><a href="' . Setting::getMyPRUrl($pr_loggedInUser) . '">ดูหน้าหลัก (My PR)</a></li>';
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                    <li class="<?php echo ($pageName == 'event') ? 'about' : ''; ?>">
                        <a href="about.php">ติดต่อเรา</a>
                    </li>
                    <?php
                    if (User::isLogin() !== true) {
                        echo '<li>';
                        echo '<a href="#" class="showLogin">เข้าสู่ระบบ</a>';
                        echo '</li>';
                    } else {
                        echo '<li>';
                        echo '<a href="#" class="cmdLogout">
            <span class="hide-hover">' . $user['name'] . '</span>
            <i class="fa fa-sign-out font-size-14"></i>
            <span class="show-hover">ออกจากระบบ</span>
            </a>';
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
        </nav>

        <?php if (User::isLogin() !== true || ($pageName !== 'admin' && $pageName !== 'owner')) { ?>
            <div class="container hidden-xs">
                <div class="row">
                    <img class="imgheader" src="images/pongyeang_header_bg.jpg">

                    <div class="container imgheader-text-container">
                        <div class="imgheader-text-title">
                            "ABOUT<br/>
                            PONGYEANG"
                        </div>
                        <div class="imgheader-text-detail pull-right font-size-16 ">
ตำบลโป่งแยงมีสภาพทางกายภาพทั่วไปเป็นภูเขา  บางส่วนเป็นที่ราบเชิงเขาและที่ราบพื้นที่บางส่วนอยู่ในความรับผิดชอบของอุทยานแห่งชาติดอยสุเทพ-ปุยและแม่สา พื้นที่ส่วนใหญ่เป็นป่าไม้และเป็นพื้นที่ราบเพาะปลูก  เหมาะแก่การทำเกษตรกรรม และการแปรรูปผลผลิตทางการเกษตร  เนื่องจากมีสภาพอากาศที่หนาวเย็น เหมาะแก่การท่องเที่ยวพักผ่อน เรียนรู้   ดังคำขวัญของโป่งแยงที่กล่าวไว้ว่า   ต้นน้ำแม่สา  ตระการตาไม้ดอก  ส่งออกพริกหวาน บ้านพักโฮมสเตย์   ประเพณีไทยม้ง  ถิ่นดงมะระหวาน
ตำนานขุนหลวง  บวงสรวงพญาแสน   ดินแดนมะแขว่นหอม  หลอมรวมวัฒนธรรม

                    </div>
                </div>
            </div>
        <?php } ?>
    </header>
