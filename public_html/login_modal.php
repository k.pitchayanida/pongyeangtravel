
<!-- Login Modal -->
<div class="modal fade" id="loginModal" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title">
               <i class="fa fa-lock"></i>
               Login
               <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
                  <i class="fa fa-times"></i>
               </button>
            </h3>
         </div>
         <form class="form-horizontal" role="form" onsubmit="return false;" data-toggle="validator" id="loginForm">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-sm-offset-2 col-sm-10" for="username">Username</label>
                  <div class="col-sm-offset-2 col-sm-8">
                     <input name="username" type="text" class="form-control" id="username" placeholder="Username" required="required">
                     <div class="help-block with-errors"></div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-offset-2 col-sm-10" for="password">Password</label>
                  <div class="col-sm-offset-2 col-sm-8">
                     <input name="password" type="password" class="form-control" id="password" placeholder="Password" required="required">
                     <div class="help-block with-errors"></div>
                  </div>
               </div>
               <div class="modal-footer" style="text-align: center;">
                  <button type="submit" class="btn btn-green font-size-16 font-thaisan" id="cmdLogin">
                     <i class="fa fa-key"></i>
                     เข้าสู่ระบบ
                  </button>
                  <button type="button" class="btn btn-white font-size-16 font-thaisan cmdForgot">
                     ลืมรหัสผ่าน ?
                  </button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Forgot Password Modal -->
<div class="modal fade" id="forgotModal" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title">
               <i class="fa fa-exclamation-circle"></i>
               คำร้องขอข้อมูลเข้าสู่ระบบ
               <button type="button" class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
                  <i class="fa fa-times"></i>
               </button>
            </h3>
         </div>
         <form class="form-horizontal" role="form" onsubmit="return false;" data-toggle="validator" id="forgotForm">
            <div class="modal-body">
               <div class="form-group">
                  <label class="col-sm-offset-2 col-sm-10" for="citizenID">หมายเลขบัตรประชาชนของผู้ใช้ระบบ</label>
                  <div class="col-sm-offset-2 col-sm-8">
                     <input name="citizenID" type="text" class="form-control" id="citizenID" placeholder="หมายเลขบัตรประชาชน 13 หลัก" pattern=".{13,13}" maxlength="13" required="required" data-pattern-error="หมายเลขบัตรประชาชนไม่ถูกต้อง">
                     <div class="help-block with-errors"></div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-offset-2 col-sm-10" for="password">อีเมล์ของผู้ใช้งานระบบ</label>
                  <div class="col-sm-offset-2 col-sm-8">
                     <input name="email" type="email" class="form-control" id="email" placeholder="example@example.com" required="required">
                     <div class="help-block with-errors"></div>
                  </div>
               </div>
               <div class="modal-footer" style="text-align: center;">
                  <button type="submit" class="btn btn-green font-size-16 font-thaisan" id="cmdSubmitForgot">
                     <i class="fa fa-check"></i>
                     ยืนยันแจ้งคำร้องลืมรหัสผ่าน
                  </button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
