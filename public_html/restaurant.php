<?php
include_once 'shared/setting.php';
$category = isset($_GET['category']) ? $_GET['category'] : -1;

$mainIndex = 2;
$sub_category = Setting::$pr_sub_category[$mainIndex];
$sub_category_key = Setting::$pr_sub_category_key[$mainIndex];

if (in_array($category, $sub_category_key)) {
    $idx = Setting::pr_sub_category_index($mainIndex, $category);
    $title = $sub_category[$idx];
    $link = 'restaurant.php?category=' . $sub_category_key[$idx];
} else {
    $category = -1;
    $title = 'ประเภทร้านอาหาร ร้านค้า อร่อยด้วยกันในโป่งแยง';
    $link = 'restaurant.php';
}
$pr = false;
$isAdmin = false;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    //$status = (isset($_GET['role']) && $_GET['role'] == 'admin') ? 'unactive' : 'Active';

    if (isset($_GET['role']) && $_GET['role'] == 'admin') {
        $status = 'unactive';
        $isAdmin = true;

        $pr = Amst::get(Setting::$section_code . '_pr', '*', array(
            'AND' => array(
                'id' => $id,
            )
        ));
    } else {
        $status = 'Active';
        $pr = Amst::get(Setting::$section_code . '_pr', '*', array(
            'AND' => array(
                'id' => $id,
                'status' => $status
            )
        ));
    }

//    $pr = Amst::get(Setting::$section_code . '_pr', '*', array(
//        'AND' => array(
//            'id' => $id,
//            //'status' => $status
//        )
//    ));

    if (!$pr) {
        header('Location: restaurant.php');
        exit();
    }

    $images = Amst::select(Setting::$section_code . '_pr_image', '*', array(
        'AND' => array(
            'pr_id' => $id,
            'status' => 'Active'
        )
    ));
    $pageLink = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $owner = User::getUserByID($pr['insertuserid']);
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Pongyeang Travel : <?php echo $title; ?></title>
    <?php
    if ($pr) {
        ?>
        <!-- SEO meta tags -->
        <meta name="keywords" content="<?php echo $pr['keyword_seo']; ?>">
        <meta name="description" content="<?php echo $pr['description_short']; ?>">
        <!-- Open Graph data -->
        <meta property="og:title" content="Pongyeang Travel : <?php echo $pr['name']; ?>"/>
        <meta property="og:type" content="article"/>
        <meta property="og:description" content="<?php echo $pr['description_short']; ?>"/>
        <meta property="og:site_name" content="Pongyeang Travel"/>
        <link rel="stylesheet" href="css/ekko-lightbox.min.css">
        <?php if ($images) {
            $imageID = $images[0]['id'];
            $imagePath = File::getPath(Setting::$section_code . '_pr_' . $imageID, 'system/');
            if (!$imagePath) {
                $imagePath = 'images/system/noimage.jpg';
            }
            echo '<meta property="og:image" content="' . $imagePath . '" />';
        }
    }
    include 'loadcss.php';
    ?>
</head>

<body class="font-thaisan">
<?php
include 'header.php';
?>
<div class="container">
    <div class="row card">
        <div class="col-xs-12">
            <div class="row header2">
                <div class="col-xs-12 border-bottom2">
                    <div class="col-xs-12 no-padding">
                        <?php
                        if ($isAdmin) {
                            ?>
                            <h2 class="font-size-20 bold">Admin จัดการ >
                                <a href="admin.php?page=managePR">จัดการ PR</a>
                                >
                                <?php
                                echo $title;
                                ?>
                            </h2>
                            <?php
                        } else {
                            ?>
                            <h1 class="font-size-20 bold">
                                <a href="restaurant.php">อร่อยด้วยกันในโป่งแยง</a> >
                                <?php
                                echo '<a href="' . $link . '">' . $title . '</a>';
                                ?>
                            </h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
            // router
            if (isset($_GET['id'])) {
                include 'public/pr/view.php';
            } else {
                if ($category != -1) {
                    $category = $mainIndex;
                    $sub_category = $idx;
                    include 'public/pr/showList.php';
                } else {
                    include 'restaurant/all.php';
                }
            }
            ?>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
</body>
</html>
