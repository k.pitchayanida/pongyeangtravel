<?php

include_once 'shared/setting.php';

?>

<!DOCTYPE html>

<html>



<head>

	<meta charset="utf-8">

	<title>Pongyeang Travel : Search Pongyeang Travel</title>

	<?php

	include 'loadcss.php';

	?>

</head>



<body class="font-thaisan">

	<?php

	include 'header.php';

	$search = isset($_GET['q']) ? trim($_GET['q']) : '';

	?>

	<div class="container" style="margin-top: 25px">

		<div class="row card">

			<div class="col-xs-12">

				<div class="row header2" style="padding: 10px;">

					<form role="search" action="search.php" method="GET">

						<div class="col-xs-12 border-bottom2" style="padding-bottom: 10px;">

							<div class="col-sm-3">

								<span class="font-size-20 bold">ค้นหาข้อมูล </span>

							</div>

							<div class="col-sm-5 col-xs-10 no-padding-right">

								<input type="text" class="form-control" name="q" placeholder="ระบุชื่อแหล่งท่องเที่ยว หรือ hashtag เพื่อค้นหา" value="<?php echo $search;?>" autofocus="autofocus" />

							</div>

							<div class="col-xs-2 no-padding-left">

								<button type="submit" class="btn btn-green">

									<i class="fa fa-search"></i>

									ค้นหา

								</button>

							</div>

						</div>

					</form>

				</div>



				<div class="font-size-14" style="margin-bottom:20px" id="prList">

					<div class="row text-center">

					</div>

					<table id="prContainer" width="100%">

						<thead style="display: none;">

							<tr><th></th></tr>

						</thead>

						<tbody>

							<?php

							$searchQuery = explode(' ', $search);
							$prs = Amst::select(Setting::$section_code.'_pr', '*', array(

								'AND' => array(

									'OR' => array(

										'name[~]' => $searchQuery,

										'keyword_hashtag[~]' => $searchQuery

									),

									'status' => 'Active'

								)

							));



							$nResult = count($prs);

							if($nResult == 0 || $search == '') {

								echo  '<tr><td>';

								echo '<div class="row" style="padding-top: 10px;">';

								echo '<div class="col-xs-12 body-yellow" style="padding: 10px;">';

								echo '<h3 class="text-center">ไม่พบข้อมูล</h3>';

								echo '</div></div>';

								echo '</td></tr>';

							} else {

								for($i = 0; $i < $nResult; $i++) {

									echo '<tr><td>';

									$pr = $prs[$i];

									$page = 'travel.php';

									if($pr['category'] == 1) {

										$page = 'hotel.php';

									} else if($pr['category'] == 2) {

										$page = 'restaurant.php';

									}



									$image = Amst::select(Setting::$section_code.'_pr_image', 'id', array(

										'pr_id' => $pr['id']

									));

									if(!$image) {

										$imgSrc = 'images/system/noimage.jpg';

									} else {

										shuffle($image);

										$imgSrc = File::getPath(Setting::$section_code.'_pr_'.$image[0], 'system/');

										if(!$imgSrc) {

											$imgSrc = 'images/system/noimage.jpg';

										}

									}

									$address = $pr['address_number'].', หมู่ที่ '.$pr['address_village'].' '.Setting::$village_list[$pr['village']].' ตรอก/ซอย '.$pr['address_lane'].', ถนน '.$pr['address_road'].' ตำบล '.$pr['address_sub_district'].' อำเภอ '.$pr['address_district'].' จังหวัด '.$pr['address_province'].', '.$pr['address_postal_code'];

									echo '<div class="row">';

									echo '<div class="col-xs-12 body-yellow article-control card" style="padding: 10px;" data-id="'.$pr['id'].'">';

									echo '<div class="col-sm-3">';

									echo '<img src="'.$imgSrc.'" class="img-responsive" padding: 0px;">';

									echo '</div>';

									echo '<div class="col-sm-9">';

									echo '<h2 class="border-bottom1 text-green">'.$pr['name'].'</h2>';

									echo '<p class="font-size-18">'.$address.'</p>';

									echo '<p class="font-size-18">เบอร์โทรศัพท์ : '.$pr['contact_telephone'].'</p>';

									echo '<p>'.$pr['description_short'].'</p>';

									echo '<p class="border-bottom1" style="padding-bottom:10px;">Hashtag: ';

									if(trim($pr['keyword_hashtag']) != '') {
										$hashtags = explode(',', trim($pr['keyword_hashtag']));

										for($i = 0; $i < count($hashtags) ; $i++) {

											$tag = trim($hashtags[$i]);

											echo '<a href="search.php?q='. str_replace('#', '', $tag).'" class="btn btn-sm btn-info font-size-14">'.$tag.'</a> ';

										}

									}

									echo '</p>';

									echo '<div class="col-sm-2">';

									echo '<p class="font-size-14">ผู้เข้าชม : '.$pr['count_view'].' ครั้ง </p>';

									$prURL =  str_replace('system', '',ROOT_URL).$page.'?category='.Setting::$pr_sub_category_key[$pr['category']][$pr['sub_category']].'&id='.$pr['id'];

									echo '</div>';

									echo '<div class="col-sm-2">';

									echo '<div class="fb-share-button" data-href="'.$prURL.'" data-layout="button_count" data-size="large" data-mobile-iframe="true">

									</div>';

									echo '</div>';

									echo '<div class="col-sm-8" style="text-align: right;">';

									echo '<a href="'.$prURL.'" class="btn btn-green font-size-14"><i class="fa fa-search"></i> อ่านเพิ่มเติม</a> ';

									echo '</div></div></div></div>';

									echo '</td></tr>';

								}

							}

							?>

						</tbody>

					</table>

				</div>

			</div>

		</div>

	</div>

	<!-- Load Facebook SDK for JavaScript -->

	<div id="fb-root"></div>

	<script>

	(function(d, s, id) {

		var js, fjs = d.getElementsByTagName(s)[0];

		if (d.getElementById(id)) return;

		js = d.createElement(s); js.id = id;

		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appID=1919203258291527";

		fjs.parentNode.insertBefore(js, fjs);

	}(document, 'script', 'facebook-jssdk'));

	</script>

	<?php include 'footer.php'; ?>

</body>

</html>

