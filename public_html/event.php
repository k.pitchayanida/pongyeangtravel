<?php
include_once 'shared/setting.php';
$category = -1;
if(isset($_GET['category'])) {
	$category = Setting::article_category_index($_GET['category']);
}
$title = array();
for($i = 0; $i < count(Setting::$article_category); $i++) {
	array_push($title, ' > <a href="event.php?category='.Setting::$article_category_key[$i].'">'.
	Setting::$article_category[$i].'</a>');
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<?php if($category == -1) {
		echo '<title>Pongyeang Travel : กิจกรรมการท่องเที่ยว</title>';
	} else {
		echo '<title>Pongyeang Travel : '.Setting::$article_category[$category].'</title>';
	}
	?>
	<?php
	include 'loadcss.php';
	?>
</head>

<body class="font-thaisan">
	<?php include 'header.php';?>
	<div class="container" style="margin-bottom: 20px;" id="manageArticle">
		<div class="row card">
			<div class="col-xs-12 ">
				<div class="row header2">
					<div class="col-xs-12 border-bottom2">
						<div class="col-xs-9 no-padding">
							<h2 class="font-size-20 bold">
								<a href="event.php">กิจกรรมการท่องเที่ยว</a>
								<?php if($category != -1 ) echo $title[$category];?>
							</h2>
						</div>
						<div class="col-xs-3" style="text-align: right;">
							<h2 class="font-size-16 bold"></h2>
						</div>
					</div>
				</div>
				<?php if($category == '0') {
					echo '<div class="row text-center" style="margin-top: 10px;">
					ลำดับการแสดงผลจาก
					<select name="order" required="required">
					<option value="all">แสดงทั้งหมดตามลำดับวันเวลากิจกรรม</option>
					<option value="new">สร้างใหม่ล่าสุด</option>
					<option value="week">กิจกรรมภายในสัปดาห์นี้</option>
					<option value="month">กิจกรรมภายในเดือนนี้</option>
					<option value="asc">เรียงลำดับ ก-ฮ</option>
					<option value="desc">เรียงลำดับ ฮ-ก</option>
					</select>
					</div>';
				}
				?>
				<div class="font-size-14" style="margin-top: 10px">
					<table id="articleContainer" width="100%">
						<thead style="display: none;">
							<tr><th></th></tr>
						</thead>
						<tbody>
							<!-- Article List Here -->
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function() {
		var $scope = $('#manageArticle');
		var table = null;
		<?php if($category == 0) { ?>
			var $order = $scope.find('select[name=order]');
			$order.on('change', function (e) {
				table.ajax.reload();
			});
			<?php } ?>

			// binding article control buttons
			$scope.on('click', '.cmdView', function(e) {
				var id = $(this).parents('.article-control').data('id');
				window.location = 'article.php?id=' + id;
			});

			initArticleList = function () {
				var $container = $scope.find('#articleContainer');
				table = $container.DataTable({
					'dom': '<tp>',
					'pagingType': 'full_numbers',
					'language': System.helper.DataTable.language,
					'processing': true,
					'serverSide': true,
					'ajax': {
						'url': 'public/article/ajax.articleList.php',
						'type': 'POST',
						'data': function(data) {
							data.category = <?php echo $category;?>
							<?php if($category == 0 ) echo ', data.order = $order.val()';?>
						}
					}
				});
				$scope.find('.dataTables_paginate').css('text-align', 'center');
				$scope.find('.dataTables_paginate').css('float', 'none');
			}

			$(document).ajaxComplete(function(){
				try{
					FB.XFBML.parse();
				}catch(ex){}
			});

			initArticleList();
		});
		</script>
		<!-- Load Facebook SDK for JavaScript -->
		<div id="fb-root"></div>
		<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appID=1919203258291527";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
		<?php include 'footer.php'; ?>
	</body>
	</html>
