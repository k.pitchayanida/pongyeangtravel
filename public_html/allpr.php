<?php
include_once 'shared/setting.php';
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Pongyeang Travel : Official Pongyeang Travel Resources</title>
	<?php
	include 'loadcss.php';
	?>
</head>

<body class="font-thaisan">
	<?php
	include 'header.php';
	?>
	<div class="container">
		<div class="row card">
			<div class="col-xs-12">
				<div class="row header2">
					<div class="col-xs-12 border-bottom2">
						<div class="col-xs-12 no-padding">
							<h1 class="font-size-20 bold">
								<a href="allpr.php">แสดงรายการข้อมูล PR แหล่งท่องเที่ยวทั้งหมดในระบบ</a>
							</h1>
						</div>
					</div>
				</div>
				<?php
				$category = -1;
				$sub_category = -1;
				include 'public/pr/showList.php';
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<?php include 'footer.php'; ?>
	</div>
</body>
</html>
