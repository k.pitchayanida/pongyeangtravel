<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <title>Pongyeang Travel : Admin</title>
   <?php
   include 'loadcss.php';
   ?>
</head>

<body class="font-thaisan">
   <?php
   include 'header.php';
   $hash = isset($_GET['id']) ? $_GET['id'] : '';
   $password = isset($_GET['pw'])? $_GET['pw'] : '';
   ?>
   <div class="container" style="margin-top: 25px; margin-bottom: 35px;">
      <div class="row">
         <div class="col-xs-12">
            <?php
            $users = User::getUser();
            $nUser = count($users);
            for($i = 0; $i < $nUser; $i++) {
               $user = $users[$i];
               if(md5($user['email']) == $hash) {
                  if(User::isLogin() == true) {
                     User::logout();
                  }
                  if($user['status'] == 'New' || $user['status'] == 'Pending') {
                     $result = User::login_server($user['username'], $password);
                     if($result == 'success') {
                        if($user['usertype'] == 'owner') {
                           header('Location: owner.php?page=managePR');
                        } else {
                           header('Location: admin.php?page=password');
                        }
                        exit();
                     } else {
                        echo 'ลิงค์สำหรับเข้าสู่ระบบไม่ถูกต้อง หรือหมดอายุ';
                     }
                  }  else {
                     echo 'ลิงค์สำหรับเข้าสู่ระบบไม่ถูกต้อง หรือหมดอายุ';
                  }
                  break;
               }
            }
            ?>
         </div>
      </div>
   </div>
   <?php include 'footer.php'; ?>
</body>
</html>
